#include <Arduino.h>
#include <main.h>
#include <TM1638.h>
#include <SerialDebug.h>
#include <TimeTrigger.h>

#include "GameLogic.h"
#include "Button.h"
#include "countdown.h"
#include "FileSystem.h"

// init LED Modul
TM1638 display(Pin_Data, PIN_Clock, PIN_Strobe);
Countdown cntd_main(0, 0, 10, 0, 0);

// Debug Level
SerialDebug dbg(ALL);
GameLogic GameLogik;
Button But;
FileSystem fileSys(ALL);

//Ticks
TimeTrigger Tick10ms(10);
TimeTrigger Tick20ms(20);
TimeTrigger Tick500ms(500);
TimeTrigger Tick5000ms(5000);

void WriteBackup(long Time1, byte Status)
{
  char buf[20];
  String TXT = String(Time1);
  TXT.toCharArray(buf, TXT.length() + 2);
  fileSys.WriteNewFile((char *)"TIME", buf);
  String TXT1 = String(Status);
  TXT1.toCharArray(buf, TXT1.length() + 2);
  fileSys.WriteNewFile((char *)"STATE", buf);
}

void setup()
{

  Serial.begin(960000);
  BtStatus Bt = But.getBtStatus(display, dbg);
  fileSys.mount();

  if (Bt.BtLow == 1 && Bt.BtHigh == 8) //Reset Time and State left and rigth Button pressed + PowerON
  {
    WriteBackup(605000, 0);
  }

  String TXT = fileSys.ReadFile((char *)"TIME");
  String stat = fileSys.ReadFile((char *)"STATE");
  if (TXT == "notExists")
  {
    WriteBackup(605000, 0);
    cntd_main.setTime(600000);
    GameLogik.setGameState(0, display);
  }
  else
  {
    long tim = TXT.toInt();
    if (tim > 5000)
    {
      tim -= 5000;
    }
    else
    {
      tim = 10;
    }

    display.setDisplayToString("bAdC0dEd");
    delay(2000);
    display.ClearDisplayVertical(End2Middle, 20);
    display.clearDisplay();

    cntd_main.setTime(tim);
    GameLogik.setGameState(stat.toInt(), display);
  }

  cntd_main.start();
}

void loop()
{

  //Tasten einlesen

  if (Tick10ms.Trigger())
  {
    BtStatus BtState = But.getBtStatus(display, dbg);
    long ti = cntd_main.getCountDown();
    if (ti < 30)
    {
      WriteBackup(605000, 0);
    }
    GameLogik.GameAblauf(BtState, display, cntd_main, dbg);
    GameLogik.refreshDisplay(display, BtState, cntd_main, dbg);
  }

  if (Tick20ms.Trigger())
  {
    GameLogik.refreshBlinkTimer();
  }

  if (Tick500ms.Trigger())
  {
  }

  if (Tick5000ms.Trigger())
  {

    byte state = GameLogik.getDigiState();
    long ti = cntd_main.getCountDown();
    if (ti < 580000)
    {
      WriteBackup(ti, state);
    }
  }
}
