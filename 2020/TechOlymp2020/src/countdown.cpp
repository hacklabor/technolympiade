#include "countdown.h"

Countdown::Countdown(int days, int hours, int min, int sec,int ms)
{
    long tmpTime = ms;
    tmpTime += sec * 1000;
    tmpTime += min * 60 * 1000;
    tmpTime += hours * 60 * 60 * 1000;
    tmpTime += days * 24 * 60 * 60 * 1000;
    cntdTime = tmpTime;
}

void Countdown::setTime(long time1){
    cntdTime=time1;
}

void Countdown::setTime(int days, int hours, int min, int sec,int ms)
{
    long tmpTime = ms;
    tmpTime += sec * 1000;
    tmpTime += min * 60 * 1000;
    tmpTime += hours * 60 * 60 * 1000;
    tmpTime += days * 24 * 60 * 60 * 1000;
    cntdTime = tmpTime;
}

void Countdown::start()
{
    startTime = millis();
}
long Countdown::getCountDown()


{
    unsigned long t=cntdTime - (millis() - startTime);
    if (t>cntdTime){
        return 0;
    }
    return t ;
}
void Countdown::showCountDown(TM1638 &Display, CNTD_STYLE style, byte startDigit, long time)
{        
        long rest = 0;
        char buf[11];
        

        int days = int(time / (24 * 60 * 60 * 1000));
        rest = time % (24 * 60 * 60 * 1000);
         
           
        int hours = int(rest / (60*60*1000));
        rest = rest % (60*60*1000);
              
        int min = int(rest / (60*1000));
        rest = rest % (60*1000);
                    
        int sec = int(rest / 1000);
                rest = rest %  1000;
                  
        int ms = rest;
        String strTime = "";
      
        sprintf(buf, "%2i%2i%2i%2i%3i ", days, hours,min,sec, ms);
        
        int dotPos[] = {255, 255, 255, 255, 255}; //255 = nicht belegt
        String displayText = "";
        
        switch (style)
        {
        case CNTD_00s00ms:

            displayText = getTheString(buf, 6, 10);
            dotPos[0] = 1;

            break;
        
        case CNTD_00s0ms:
            displayText = getTheString(buf, 6, 9);
            dotPos[0] = 1;
            break;
            case CNTD_00min00s00ms:
            displayText = getTheString(buf, 4, 10);
            dotPos[0] = 1;
            dotPos[1] = 3;
            break;
            case CNTD_00min00s0ms:
            displayText = getTheString(buf, 4, 9);
            
              dotPos[0] = 1;
            dotPos[1] = 3;
            break;
            

        default:
            break;
        }
        Display.setDisplayToString(" ",7);
Display.setDisplayToString(" ",0);
        drawDisplay(Display, startDigit, displayText, dotPos);
    
}

void Countdown::drawDisplay(TM1638 &Display, byte DisplayStartDigit, String &anzeige, int dotPos[])
{
   
    byte dotCount = 0;
    byte dot = dotPos[0];
    byte aktPos = 0;
    for (unsigned int i = 0; i < anzeige.length(); i++)
    {
        aktPos = 7 - DisplayStartDigit + i;
         if (aktPos == 8)
        {
            break;
        }
        if (i == dot)
        {
            Display.setDisplayDigit(anzeige[i], aktPos, true);
            dotCount++;
            dot = dotPos[dotCount];
        }
        else
        {
            Display.setDisplayDigit(anzeige[i], aktPos, false);
        }
    }
   
}

String Countdown::getTheString(const char *str, int start, int end)
{
    String txt = "";
    for (int i = start; i < end; i++)
    {
        txt += str[i];
    }
    return txt;
}
