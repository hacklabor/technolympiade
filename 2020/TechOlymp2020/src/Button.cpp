#include <Arduino.h>
#include <Button.h>

Button::Button()
{
}

Button::~Button()
{
    lastBtValue = 0;
}
BtStatus Button::getBtStatus(TM1638 &display, SerialDebug &dbg)
{
    BtStatus state;
    byte ButtonByte = display.getButtons();

    state.BtHigh = ButtonBitTurn((ButtonByte & 0x0F));
    state.BtLow = ButtonBitTurn((ButtonByte & 0xF0) >> 4);
    if (ButtonByte == 0)
    {
        state.anyBtpressed = false;
    }
    else
    {
        state.anyBtpressed = true;
    }

    for (int i = 0; i < 8; i++)
    {

        state.Bt[i] = singleBtState((state.BtHigh * 16 + state.BtLow), i);
    }

    if (lastBtValue != ButtonByte)
    {
        state.BtChange = true;
        lastBtValue = ButtonByte;
    }
    else
    {
        state.BtChange = false;
    }
    printBtState(state, dbg);
    return state;
}

byte Button::ButtonBitTurn(byte But)
{
    byte ergebniss = 0;
    if ((But & 1) > 0)
    {
        ergebniss += 8;
    }

    if ((But & 2) > 0)
    {
        ergebniss += 4;
    }

    if ((But & 4) > 0)
    {
        ergebniss += 2;
    }

    if ((But & 8) > 0)
    {
        ergebniss += 1;
    }
    return ergebniss;
}

boolean Button::singleBtState(byte But, byte ButtonNr)
{

    if (((But >> ButtonNr) & 1) == 0)
    {
        return false;
    }
    else
    {
        return true;
    }
}

void Button::printBtState(BtStatus state, SerialDebug &dbg)

{
    if (!state.BtChange)
    {
        return;
    }

    dbg.print("HighByte", String(state.BtHigh), DEBUG);
    dbg.print("LowByte", String(state.BtLow), DEBUG);
    dbg.print("AnyButtonPressed",String(state.anyBtpressed),DEBUG);
    for (int i = 0; i < 8; i++)
    {
        dbg.print("Button " + String(i), String(state.Bt[i]), DEBUG);
    }
}
