#include "GameLogic.h"

GameLogic::GameLogic(/* args */)
{
}

/**
 * @brief 
 * 
 * @param BtState 
 * @param dbg 
 */

void GameLogic::GameAblauf(BtStatus &BtState, TM1638 &display, Countdown &cntd_main, SerialDebug &DEBUG) // Game Ablauf; umschalten des Game Status nach Bedingungen
{

    SetTimeCondition(display, DEBUG); //Dynamische Zuordnung von Zeiten und Buchstaben
    bool LB, HB;

    // Timout-- bei positiver Flanke
    //----------------------------------------------------------------
    if (TimeOutTakt && !Flanke)
    {
        if (TimeOUT > 0)
        {
            if (!BtState.anyBtpressed || GameStatus != STS_GAME_TimeOut)
                TimeOUT--;
        }

        Flanke = true;
    }

    if (!TimeOutTakt)
    {
        Flanke = false;
    }
    //----------------------------------------------------------------

    // Update Game Status
    //----------------------------------------------------------------
    switch (GameStatus)
    {
    //----------------
    case STS_GAME_TimeOut: //Timout bei Eingabe händeln
        if (TimeOUT == 0 && !BtState.anyBtpressed)
        {
            GameStatus = STS_GAME_WaitStart;
        }

        break;

    //----------------
    case STS_GAME_WaitStart:
        if (BtState.anyBtpressed)
        {
            GameStatus = STS_GAME_CheckInput;
        }

        break;

    //----------------
    case STS_GAME_CheckInput: //Eingabe der Button Abfragen und Auswerten

        LB = checkInput(BtState.BtLow, checkLB);
        HB = checkInput(BtState.BtHigh, checkHB);
        if (DigiState == 0)
        {
            if (HB) // erster Buchstabe
            {
                setNextLevel(0xb, 0xa, 1, "b", display);
            }
        }
        else if (DigiState == 1)
        {
            if (LB) // Zweiter Buchstabe
            {
                setNextLevel(0xD, 0xC, 3, "bA", display);
            }
        }
        else
        {
            if (LB & HB) // Ab dritten Buchstaben
            {
                switch (DigiState)
                {

                case 0x3 ... 0xE:
                    setNextLevel(0x0, 0xD, 0xF, "bAdC", display);
                    break;
                case 0xF ... 0x2F:
                    setNextLevel(0xE, 0xD, 0x3F, "bAdC0d", display);
                    break;
                case 0x3F ... 0xFE:
                    setNextLevel(0xE, 0xD, 0xFF, "bAdC0dEd", display);
                    break;
                }
                display.setDisplayToString(PW);
                KRaider(display);
                GameStatus = STS_GAME_Points;
            }
        }
        if (TimeOUT == 0)
        {
            GameStatus = STS_GAME_TimeOut;
        }
        break;

    //----------------
    case STS_GAME_Points: // Punkte Anzeige

        if (TimeOUT == 0)
        {

            DEBUG.print("win", String(GameStatus), ALL);
            if (DigiState == 0xff)
            {
                winTime = cntd_main.getCountDown();
                GameStatus = STS_GAME_Win;
            }
            else
            {
                GameStatus = STS_GAME_WaitStart;
            }
        }
        break;

    default:
        break;
    }
}

void GameLogic::setNextLevel(byte HB, byte LB, byte state, String lastPasswd, TM1638 &display)
{
    DigiState = state;
    PW = lastPasswd;
    checkHB = HB;
    checkLB = LB;
    display.setDisplayToString(PW);
    KRaider(display); //Lauflicht wenn match
    GameStatus = STS_GAME_Points;
}

void GameLogic::SetTimeCondition(TM1638 &display, SerialDebug &DEBUG) //Händeln der Zeiten; Texte; Inputs je nach Game Status
{

    if (GameStatus != OldGameStatus) // nur einmal bei Statuswechsel
    {
        TimeOUT = 8;
        OldGameStatus = GameStatus;
        DEBUG.print("GameStatus", String(GameStatus), ALL);
        display.setLEDs(0);
        display.clearDisplay();
    }
    //---------------------------------------------------------------------
    // Setze Variablen in Abhängigkeit des Status
    switch (GameStatus)
    {

    case STS_GAME_Points: //Zeit x8 für Anzeige der Punkte

        TimeOutTakt = blink_80ms;

        break;

    case STS_GAME_TimeOut: // TimeOut für Error Anzeige x8
        switch (DigiState)
        {
        case 0x0 ... 0x2: //Level 1 + 2
            TimeOutTakt = blink_320ms;

            break;
        case 0x3 ... 0xE: //Level 3
            TimeOutTakt = blink_640ms;

            break;
        case 0xF ... 0x2F: //Level 4
            TimeOutTakt = blink_1280ms;
            break;

        case 0x3F ... 0xFE: //Level 5
            TimeOutTakt = blink_1280ms;
            break;
        case 0xFF: //Level 5
            break;
        }
        break;

    case STS_GAME_CheckInput: // TimeOut für Eingabe x8
        switch (DigiState)
        {
        case 0x0: //Level 1
            TimeOutTakt = blink_1280ms;
            break;
        case 0x1: //Level 2
            TimeOutTakt = blink_1280ms;
            break;
        case 0x3 ... 0xE: //Level 3
            TimeOutTakt = blink_1280ms;
            break;
        case 0xF ... 0x2F: //Level 4
            TimeOutTakt = blink_80ms;
            break;
        case 0x3F ... 0xFE: //Level 5
            TimeOutTakt = blink_40ms;
            break;
        case 0xFF: // geschaft
            TimeOutTakt = blink_20ms;
            break;
        }
        break;

    default:
        break;
    }
}

/*********************************************************************/

/**
 * @brief 
 * 
 * @param display 
 * @param BtState 
 * @param cntd_main 
 * @param dbg 
 */

void GameLogic ::refreshDisplay(TM1638 &display, BtStatus BtState, Countdown &cntd_main, SerialDebug &dbg) // Händeln was auf dem Display angezeigt wird

{

    long time1 = cntd_main.getCountDown();
    String inputField = "";
    if (DigiState < 3)
    {
        inputField += PW + "_       ";
    }
    else
    {
        inputField += PW + "__      ";
    }
    if ((time1 < 1) && (GameStatus != STS_GAME_Win))
    {
        GameStatus = STS_GAME_GameOver;
    }
    switch (GameStatus)
    {

    case STS_GAME_TimeOut:

        displayTime(display, TimeOUT); // Anzeige der 8 LEDS und runterzählen Timout
        displayToggle(display, blink_640ms, String("Error"), String("bAdCodEd"));
        break;

    case STS_GAME_WaitStart:
        if (blink_1280ms && !blink_1280ms)
        {

            display.setDisplayToString(inputField);
        }
        else
        {
            cntd_main.showCountDown(display, CNTD_00min00s00ms, 6, time1);
        }

        break;

    case STS_GAME_Win:

        // add Bonuspoints
        if (winTime > 60000)
        {
            BonusPoints++;
        }
        if (winTime > 120000)
        {
            BonusPoints++;
        }
        FinalAnimation(display, cntd_main, winTime);
        break;
    case STS_GAME_GameOver:
        FinalAnimation(display, cntd_main, 0);
        break;
    case STS_GAME_CheckInput:
        displayTime(display, TimeOUT);  // Anzeige der 8 LEDS und runterzählen Timout
        display.setDisplayToString(PW); // Anzeige der bereits korekt eingegeben Buchstabe

        if (blink_320ms)
        {

            display.setDisplayToString(inputField);
        }
        else
        {
            if (DigiState == 0) //Level 1
            {

                display.setDisplayDigit(BtState.BtHigh, PW.length(), false); // Aktullen Button Status High Byte
            }
            else if (DigiState == 1) //Level 2
            {
                display.setDisplayDigit(BtState.BtLow, PW.length(), false); // Aktullen Button Status Low Byte
            }
            else // >Level 2
            {
                display.setDisplayDigit(BtState.BtHigh, PW.length(), false);    // Aktullen Button Status High Byte
                display.setDisplayDigit(BtState.BtLow, PW.length() + 1, false); // Aktullen Button Status Low Byte
            }
        }

        break;

    case STS_GAME_Points: // Show Points
        DisplayPoints(display);
        break;

    default:
        break;
    }
}

void GameLogic::DisplayPoints(TM1638 &display)
{
    display.setLEDs(0);
    int points = BonusPoints;
    String txt = "Points";
    for (int i = 0; i < 8; i++)
    {
        if ((DigiState >> i) & 1)
        {
            points++;
        }
    }

    if (points < 10)
    {
        txt += " ";
    }
    txt += points;
    display.setDisplayToString(txt);
}

void GameLogic::KRaider(TM1638 &display) //Lauflichtanimation
{
    int LED = 1;
    for (int i = 0; i < 8; i++)
    {
        display.setLEDs(LED << i);
        delay(50);
    }
    LED = 128;
    for (int i = 0; i < 8; i++)
    {
        display.setLEDs(LED >> i);
        delay(50);
    }
}

/*********************************************************************/

/**
 * @brief 
 * 
 */

int GameLogic ::refreshBlinkTimer() // erzeugen der Unterschiedlichen Zeittakte

{
    int blinkTimeBase1 = blinkTimeBase;
    blink_20ms = blinkTimeBase1 & 1;
    blink_40ms = (blinkTimeBase1 >> 1) & 1;
    blink_80ms = (blinkTimeBase1 >> 2) & 1;
    blink_160ms = (blinkTimeBase1 >> 3) & 1;
    blink_320ms = (blinkTimeBase1 >> 4) & 1;
    blink_640ms = (blinkTimeBase1 >> 5) & 1;
    blink_1280ms = (blinkTimeBase1 >> 6) & 1;
    blink_2560ms = (blinkTimeBase1 >> 7) & 1;

    blinkTimeBase++;
    return blinkTimeBase;
}

void GameLogic::FinalAnimation(TM1638 &display, Countdown &cntd_main, long time) // Win or Loose Animation
{
    while (true)
    {
        display.clearDisplay();
        cntd_main.showCountDown(display, CNTD_00min00s00ms, 6, time);
        delay(1000);
        display.clearDisplay();
        DisplayPoints(display);
        delay(1000);
    }
}

void GameLogic ::displayTime(TM1638 &display, int Time) // Anzeige des Eingabe Timeouts
{
    switch (Time)
    {
    case 8:
        display.setLEDs(255);
        break;
    case 7:
        display.setLEDs(127);
        break;
    case 6:
        display.setLEDs(63);
        break;
    case 5:
        display.setLEDs(31);
        break;
    case 4:
        display.setLEDs(15);
        break;
    case 3:
        display.setLEDs(7);
        break;
    case 2:
        display.setLEDs(3);
        break;
    case 1:
        display.setLEDs(1);
        break;
    case 0:
        display.setLEDs(0);
        break;

    default:
        break;
    }
}

//private Fuctions

/**
 * @brief 
 * 
 * @param display 
 * @param blink 
 * @param txt 
 */

void GameLogic ::displayBlink(TM1638 &display, boolean blink, String txt) // Displayanzeige blinkend
{
    if (blink)
    {
        display.setDisplayToString(txt);
    }
    else
    {
        display.clearDisplay();
    }
}
void GameLogic ::displayToggle(TM1638 &display, boolean blink, String txt1, String txt2) // Displayanzeige wechselt zwischen zwei Texten
{
    display.clearDisplay();
    if (blink)
    {
        display.setDisplayToString(txt1);
    }
    else
    {
        display.setDisplayToString(txt2);
    }
}

bool GameLogic ::checkInput(byte wert1, byte wert2) // Vergleiche zwei Bytes
{

    if (wert1 == wert2)
    {
        return true;
    }

    return false;
}
byte GameLogic::getDigiState()
{
    return DigiState;
}

void GameLogic::setGameState(long _digiState, TM1638 &display)
{
    DigiState = _digiState;
    switch (DigiState)
    {
    case 0x0:
        setNextLevel(0xB, 0xA, 0x0, "", display);
        break;
    case 0x1:
        setNextLevel(0xB, 0xA, 0x1, "B", display);
        break;
    case 0x3 ... 0xE:
        setNextLevel(0xD, 0xC, 0x3, "bA", display);
        break;
    case 0xF ... 0x2F:
        setNextLevel(0x0, 0xD, 0xF, "bAdC", display);
        break;
    case 0x3F ... 0xFE:
        setNextLevel(0xE, 0xD, 0x3F, "bAdC0d", display);
        break;
    }
}