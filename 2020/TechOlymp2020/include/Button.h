#pragma once

#include <Arduino.h>
#include <SerialDebug.h>
#include <TM1638.h>


struct BtStatus
    {
        byte BtLow;
        byte BtHigh;
        boolean Bt[8];
        boolean BtChange;
        boolean anyBtpressed;
    };


    /**
     * @brief jjjjj
     * @parameter 
     * 
     */


class Button
{
private:

byte lastBtValue;




public:
   
    Button();
    ~Button();
    BtStatus getBtStatus(TM1638 &display, SerialDebug &dbg);
    byte ButtonBitTurn(byte But);
    boolean singleBtState(byte But, byte ButtonNr);
    void printBtState(BtStatus state, SerialDebug &dbg);
  
};


