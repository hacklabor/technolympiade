#pragma once
#include <Arduino.h>
#include <TM1638.h>
#include "Button.h"
#include "SerialDebug.h"
#include "countdown.h"

enum STS_GAME
{

    STS_GAME_TimeOut,
    STS_GAME_WaitStart,
    STS_GAME_CheckInput,
    STS_GAME_GameOver,
    STS_GAME_Points,
     STS_GAME_Win,

};

class GameLogic
{
private:
    int TimeOUT = 0;
    boolean blink_20ms = false;
    boolean blink_40ms = false;
    boolean blink_80ms = false;
    boolean blink_160ms = false;
    boolean blink_320ms = false;
    boolean blink_640ms = false;
    boolean blink_1280ms = false;
    boolean blink_2560ms = false;
    boolean TimeOutTakt = false;

    boolean Flanke = false;

    int BonusPoints =0;
    String PW = "";
    String PointTXT ="";
    byte checkHB =  0xB;
    byte checkLB =0xA;
    long winTime =0;
    int blinkTimeBase = 0;
    byte DigiState = 0x00;
   // byte DigiState = 0x3F;
    STS_GAME GameStatus = STS_GAME_WaitStart;
    STS_GAME OldGameStatus = STS_GAME_TimeOut;
    
    void displayBlink(TM1638 &display, boolean blink, String txt); // Displayanzeige blinkend
    void displayToggle(TM1638 &display, boolean blink, String txt1, String txt2); // Displayanzeige wechselt zwischen zwei Texten
    bool checkInput(byte byte1, byte byte2); // Vergleiche zwei Bytes
    void KRaider(TM1638 &display); //Lauflichtanimation
    void SetTimeCondition(TM1638 &display, SerialDebug &DEBUG); //Händeln der TimeOuts, Händeln Gamestatus Wechsel
    void displayTime(TM1638 &display, int Time); // Anzeige des Eingabe Timeouts
    void DisplayPoints(TM1638 &display); // Punkte Anzeige
    void FinalAnimation(TM1638 &display,Countdown &cntd_main,long time);
    void setNextLevel(byte HB, byte LB,byte state,String lastPasswd,TM1638 &display);

public:
    GameLogic();
    void refreshDisplay(TM1638 &display, BtStatus BtState, Countdown &cntd_main, SerialDebug &dbg); // Händeln was auf dem Display angezeigt wird
    void GameAblauf(BtStatus &BtState,TM1638 &display,Countdown &cntd_main, SerialDebug &DEBUG);  // Game Ablauf; umschalten des Game Status nach Bedingungen
    int refreshBlinkTimer(); // erzeugen der Unterschiedlichen Zeittakte
    byte getDigiState();
    void setGameState(long _digiState,TM1638 &display);
};
