#pragma once

#include <Arduino.h>
#include <TM1638.h>


enum CNTD_STYLE
{
    CNTD_00s00ms = 0,
    CNTD_00s0ms = 1,
    CNTD_00min00s00ms = 2,
    CNTD_00min00s0ms = 3,
    CNTD_00h00min00s0ms = 4,
    CNTD_00h00min00s = 5,
    CNTD_00h00min = 6,
    CNTD_00d00h00min00s = 6,
    CNTD_00d00h00min = 7,
};

class Countdown
{
private:
    unsigned long startTime = 0;
    unsigned long cntdTime = 0;


     String getTheString (const char* str, int start, int end);

public:
    Countdown(int day, int hours, int min, int sec,int ms);
    void start();
    long getCountDown();
    void showCountDown(TM1638 &Display, CNTD_STYLE style, byte startDigit,long time);
    void drawDisplay(TM1638 &Display,byte DisplayStartDigit , String &anzeige,int dotPos[] );
    void setTime(int days, int hours, int min, int sec,int ms);
    void setTime(long time1);
};
