# Aufgabe Technolympiade 2021

*Ansible Playbook zum Aufsetzen beliebig vieler Linux-VMs, die als Spielfläche für die CTF-artige Aufgabe dienen*

Aufruf mit:

```bash
ansible-playbook technolympiade.yml -i inventories/production/hosts.yml
```
