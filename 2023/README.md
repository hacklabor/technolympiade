# Aufgabe Technolympiade 2023

*Programmier-Challenge mit 4x4 LED Matrix*

**Ausgangssituation:** Die Jugendlichen finden vor sich einen Laptop mit geöffneter Arduino IDE. Die challenge.ino aus dem Git-Repo ist bereits geöffnet, die FastLED library ist installiert, das Device ist mit dem Laptop verbunden und zeigt eine bunte Animation an, die Interesse wecken soll. Der Code ist funktionsfähig und muss so vervollständigt und korrigiert werden, wie es die jeweilige Aufgabe erfordert. Alles weitere steht auf dem Aufgabenblatt. Sinn der Übung ist es, etwas über RGB-Farbwerte und das Programmieren von LEDs zu lernen. Das Device können die Jugendlichen danach mit nach Hause nehmen. Wir gehen derzeit von 35 Jugendlichen und Devices aus.

Der Aufgabentext und Hinweise finden sich neben der Aufgabe im `challenge` Verzeichnis. Die ersten zwei Seiten der PDF werden vor der Aufgabe an die Teilnehmenden verteilt. Die übrigen Seiten stellen ein Handbuch dar, welches die Jugendlichen zusammen mit dem Gerät mit nach Hause nehmen. Es erklärt weitere Grundlage und die Installation der Arduino IDE.
