#include <Arduino.h>
#include <FastLED.h>

#define DATA_PIN 4          // GPIO pin connected to the LED strip
#define NUM_LEDS 16         // Number of LEDs in the strip
#define LED_TYPE WS2812B    // LED strip type
#define BRIGHTNESS 100      // Desired brightness level (0-255)

CRGB leds[NUM_LEDS];

void setup()
{
    Serial.begin(115200);
    pinMode(LED_BUILTIN, OUTPUT);

    FastLED.addLeds<LED_TYPE, DATA_PIN, GRB>(leds, NUM_LEDS);
    FastLED.setBrightness(BRIGHTNESS);
}
//         ____                        __
//        / __ \___  ____ ____  ____  / /_  ____  ____ ____  ____
//       / /_/ / _ \/ __ `/ _ \/ __ \/ __ \/ __ \/ __ `/ _ \/ __ \
//      / _, _/  __/ /_/ /  __/ / / / /_/ / /_/ / /_/ /  __/ / / /
//     /_/ |_|\___/\__, /\___/_/ /_/_.___/\____/\__, /\___/_/ /_/
//                /____/                       /____/
//

void rainbow() {
    // Rainbow effect
    static uint8_t hue = 0;
    fill_rainbow(leds, NUM_LEDS, hue, 2);
    hue++;

    FastLED.show();
    delay(10);
}

// ******************************************************************************************************************
//         ___         ____            __            ___
//        /   | __  __/ __/___ _____ _/ /_  ___     <  /
//       / /| |/ / / / /_/ __ `/ __ `/ __ \/ _ \    / /
//      / ___ / /_/ / __/ /_/ / /_/ / /_/ /  __/   / /
//     /_/  |_\__,_/_/  \__, /\__,_/_.___/\___/   /_/
//                     /____/
//
//     Übertrage diesen bereits fertigen Code auf die LED-Matrix, sodass die erste LED rot leuchtet.
//
void aufgabe1() {
    leds[0] = CRGB(255, 0, 0); // Setze die erste LED auf Rot (R=255, G=0, B=0)
    FastLED.show(); // Sendet die Daten an den LED-Strip
    delay(1000); // Pause für eine Sekunde
}

// ******************************************************************************************************************
//         ___         ____            __            ___
//        /   | __  __/ __/___ _____ _/ /_  ___     |__ \
//       / /| |/ / / / /_/ __ `/ __ `/ __ \/ _ \    __/ /
//      / ___ / /_/ / __/ /_/ / /_/ / /_/ /  __/   / __/
//     /_/  |_\__,_/_/  \__, /\__,_/_.___/\___/   /____/
//                     /____/
//
//     Ändere den Code aus Aufgabe 1, sodass die erste LED blau leuchtet.
//
void aufgabe2() {
    leds[0] = CRGB(0, 0, 255); // Setze die erste LED auf Blau (R=0, G=0, B=255)
    FastLED.show();
    delay(1000);
}

// ******************************************************************************************************************
//         ___         ____            __            _____
//        /   | __  __/ __/___ _____ _/ /_  ___     |__  /
//       / /| |/ / / / /_/ __ `/ __ `/ __ \/ _ \     /_ <
//      / ___ / /_/ / __/ /_/ / /_/ / /_/ /  __/   ___/ /
//     /_/  |_\__,_/_/  \__, /\__,_/_.___/\___/   /____/
//                     /____/
//
//     Lasse nun die letzte LED der Matrix grün leuchten.
//
void aufgabe3() {
    leds[15] = CRGB(0, 255, 0);
    FastLED.show();
    delay(1000);
}

// ******************************************************************************************************************
//         ___         ____            __            __ __
//        /   | __  __/ __/___ _____ _/ /_  ___     / // /
//       / /| |/ / / / /_/ __ `/ __ `/ __ \/ _ \   / // /_
//      / ___ / /_/ / __/ /_/ / /_/ / /_/ /  __/  /__  __/
//     /_/  |_\__,_/_/  \__, /\__,_/_.___/\___/     /_/
//                     /____/
//
//     Korrigiere den Code, sodass auf der Matrix ein lila X angezeigt wird
//
void aufgabe4() {
    leds[0] = CRGB(128, 0, 128);
    leds[3] = CRGB(128, 0, 128);
    leds[5] = CRGB(128, 0, 128);
    leds[6] = CRGB(128, 0, 128);
    leds[9] = CRGB(128, 0, 128);
    leds[10] = CRGB(128, 0, 128);
    leds[12] = CRGB(128, 0, 128);
    leds[15] = CRGB(128, 0, 128);

    FastLED.show();
    delay(1000);
    FastLED.clear(); // Schaltet alle LEDs aus
}

// ******************************************************************************************************************
//         ___         ____            __            ______
//        /   | __  __/ __/___ _____ _/ /_  ___     / ____/
//       / /| |/ / / / /_/ __ `/ __ `/ __ \/ _ \   /___ \
//      / ___ / /_/ / __/ /_/ / /_/ / /_/ /  __/  ____/ /
//     /_/  |_\__,_/_/  \__, /\__,_/_.___/\___/  /_____/
//                     /____/
//
//     Lasse nun abwechselnd ein lila X und ein gelbes O für jeweils eine halbe Sekunde aufleuchten
//
void aufgabe5() {
    leds[0] = CRGB(128, 0, 128);
    leds[3] = CRGB(128, 0, 128);
    leds[5] = CRGB(128, 0, 128);
    leds[6] = CRGB(128, 0, 128);
    leds[9] = CRGB(128, 0, 128);
    leds[10] = CRGB(128, 0, 128);
    leds[12] = CRGB(128, 0, 128);
    leds[15] = CRGB(128, 0, 128);

    FastLED.show();
    delay(500);      // Pause für eine halbe Sekunde
    FastLED.clear(); // Schaltet alle LEDs aus
    
    leds[1] = CRGB(255, 255, 0);
    leds[2] = CRGB(255, 255, 0);
    leds[4] = CRGB(255, 255, 0);
    leds[7] = CRGB(255, 255, 0);
    leds[8] = CRGB(255, 255, 0);
    leds[11] = CRGB(255, 255, 0);
    leds[13] = CRGB(255, 255, 0);
    leds[14] = CRGB(255, 255, 0);
    
    FastLED.show();
    delay(500);      // Pause für eine halbe Sekunde
    FastLED.clear(); // Schaltet alle LEDs aus
    
}

// ******************************************************************************************************************

void loop() {

    // Entferne die //, um die jeweilige Funktion bzw. Aufgabe zu aktivieren
    // alle anderen Funktionen müssen mit // auskommentiert werden

    rainbow();
    //aufgabe1();
    //aufgabe2();
    //aufgabe3();
    //aufgabe4();
    //aufgabe5();

}
